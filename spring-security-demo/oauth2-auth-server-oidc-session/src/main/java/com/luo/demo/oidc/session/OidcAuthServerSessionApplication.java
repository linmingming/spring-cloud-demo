package com.luo.demo.oidc.session;

import com.luo.demo.security.authserver.anno.EnableOidcAuthorizationServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * OIDC Server启动类
 *
 * @author luo
 * @date 2022-02-17
 */
@EnableOidcAuthorizationServer
@SpringBootApplication
public class OidcAuthServerSessionApplication {

	public static void main(String[] args) {
		SpringApplication.run(OidcAuthServerSessionApplication.class, args);
	}

}