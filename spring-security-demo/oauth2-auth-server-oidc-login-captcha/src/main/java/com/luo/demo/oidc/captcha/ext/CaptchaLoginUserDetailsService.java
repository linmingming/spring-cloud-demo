package com.luo.demo.oidc.captcha.ext;

import com.google.code.kaptcha.Constants;
import com.luo.demo.security.authserver.handler.login.UniLoginUserDetails;
import com.luo.demo.security.authserver.handler.login.UniLoginUserDetailsService;
import com.luo.demo.security.authserver.utils.HttpContextUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 验证码登录 - 用户查询及认证服务 - 账号、密码、验证码验证实现<br/>
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-03
 */
@Component
public class CaptchaLoginUserDetailsService implements UniLoginUserDetailsService {
    /**
     * 登录表单常量定义
     */
    private static final String USERNAME_PARAMETER = "username";
    private static final String PASSWORD_PARAMETER = "password";
    private static final String CAPTCHA_PARAMETER = "captcha";

    /**
     * Spring Security UserDetailsService实现
     */
    private UserDetailsService userDetailsService;
    /**
     * 密码编码器
     */
    private PasswordEncoder passwordEncoder;

    public CaptchaLoginUserDetailsService(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
    }

    //该方法直接复用UniLoginUserDetailsPasswordMatcherService同方法（直接复用已有UserDetailsService）
    @Override
    public UniLoginUserDetails loadUserByAuthParams(Map<String, String> authParams) throws UsernameNotFoundException {
        //认证参数验证
        this.validateAuthParams(authParams);

        //获取用户名参数
        String username = authParams.get(USERNAME_PARAMETER);

        //调用SpringSecurity UserDetailsService查询用户
        UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

        //转换SpringSecurity UserDetails为UniLoginUserDetails
        return new UniLoginUserDetails(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities(), null);
    }


    @Override
    public void authenticateUser(Map<String, String> authParams, UniLoginUserDetails uniLoginUserDetails) throws AuthenticationException {
        /** 比较验证码是否一致 */
        String captchaInSession = (String) HttpContextUtils.getExistSession().getAttribute(Constants.KAPTCHA_SESSION_KEY);
        String captchaParam = authParams.get(CAPTCHA_PARAMETER);
        //若不一致，则抛出异常
        if (!captchaParam.equals(captchaInSession)) {
            throw new BadCredentialsException("验证码无效");
        }
        /** 比较密码是否一致 */
        Boolean passwordMatch = this.passwordEncoder.matches(authParams.get(PASSWORD_PARAMETER), uniLoginUserDetails.getPassword());
        //若不一致，则抛出异常
        if (!passwordMatch) {
            throw new BadCredentialsException("密码不正确");
        }

        //若验证通过，则移除缓存验证码
        HttpContextUtils.getExistSession().removeAttribute(Constants.KAPTCHA_SESSION_KEY);
    }

    /**
     * 验证认证参数（是否包含对应参数）
     *
     * @param authParams 认证参数
     */
    private void validateAuthParams(Map<String, String> authParams) {
        //验证账号、密码相关表单参数非空
        if (!authParams.containsKey(USERNAME_PARAMETER) || !authParams.containsKey(PASSWORD_PARAMETER) || !authParams.containsKey(CAPTCHA_PARAMETER)) {
            throw new UsernameNotFoundException("登录表单信息不完整");
        }
    }

}
