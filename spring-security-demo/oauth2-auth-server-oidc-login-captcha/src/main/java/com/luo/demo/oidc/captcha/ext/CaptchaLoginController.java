package com.luo.demo.oidc.captcha.ext;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 验证码登录 - controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@Controller
@RequestMapping("/captcha")
@Slf4j
public class CaptchaLoginController {

    @Resource
    private Producer captchaProducer;

    @Resource
    private Producer captchaProducerMath;


    @GetMapping("/login")
    public String login(Model model) {
        return "/login_captcha";
    }

    /**
     * 验证码生成
     */
    @GetMapping(value = "/captchaImage")
    public void getKaptchaImage(HttpServletRequest request, HttpServletResponse response) {
        ServletOutputStream out = null;
        try {
            //获取session
            HttpSession session = request.getSession();
            //设置响应头
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            response.setContentType("image/jpeg");

            //获取请求参数
            String type = request.getParameter("type");
            //前端渲染的文本
            String renderText = null;
            //实际对应的文本结果
            String resultText = null;
            //图片对象
            BufferedImage bi = null;

            //算术验证码类型，如 8*9
            if ("math".equals(type)) {
                //拆分算术表达式和结果
                String mathText = captchaProducerMath.createText();
                renderText = mathText.substring(0, mathText.lastIndexOf(KaptchaMathExpTextCreator.MATH_SEPARATOR));
                resultText = mathText.substring(mathText.lastIndexOf(KaptchaMathExpTextCreator.MATH_SEPARATOR) + 1);
                bi = captchaProducerMath.createImage(renderText);
            }
            //字符串验证码类型，如 abcdef
            else if ("char".equals(type)) {
                renderText = resultText = captchaProducer.createText();
                bi = captchaProducer.createImage(renderText);
            }
            /** 在session中设置验证码结果 */
            session.setAttribute(Constants.KAPTCHA_SESSION_KEY, resultText);

            //将验证码图片写出响应流
            out = response.getOutputStream();
            ImageIO.write(bi, "jpg", out);
            out.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}


