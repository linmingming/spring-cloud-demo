package com.luo.demo.security.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 页面controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@Controller
@Slf4j
public class PageController {
    //
    //@GetMapping("/login")
    //public String loginPage() {
    //    return "/login";
    //}
    //
    //@GetMapping("/logout")
    //public String logoutPage() {
    //    return "/logout";
    //}

    @GetMapping("/")
    public String index(Model model, Authentication authentication) {
        log.info("cur Authentication: {}", authentication);
        String welcomeMsg = "主页 - " + authentication.getName() + "!";
        log.info("cur welcomeMsg: {}", welcomeMsg);
        model.addAttribute("welcomeMsg", welcomeMsg);
        return "/main";
    }
}
