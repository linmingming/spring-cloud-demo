/*
 * Copyright 2020-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.luo.demo.security.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * Security configuration for the main application.
 *
 * @author Josh Cummings
 */
@Configuration
public class SecurityConfig implements WebMvcConfigurer {

    /** ================================================================================ */
    /** ==================== 自定义form页面 =============================================== */
    /** ================================================================================ */

    /**
     * 自定义登录、登出界面
     *
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

            //DefaultLoginPageGeneratingFilter
            //DefaultLogoutPageGeneratingFilter
        //http
        //    .formLogin(form -> form
        //            .loginPage("/login").permitAll()
        //    )
        //    .logout(logout -> logout
        //            .logoutUrl("/logout")
        //    )
        //    .authorizeHttpRequests(authorize -> authorize
        //            .anyRequest().authenticated()
        //    );
            http.authorizeHttpRequests()
                    .antMatchers("/login").permitAll()
                    .anyRequest().authenticated()
                 .and().formLogin()
                    .loginPage("/login")
                 .and().logout()
                     .logoutUrl("/logout")
            ;
            return http.build();
     }

     //TODO 自定义UserDetailsService
    //@Bean
    //UserDetailsService users() {
    //    // @formatter:off
    //    return new InMemoryUserDetailsManager(
    //            User.withUsername("luo")
    //                    .password("{noop}123456")
    //                    .authorities("app")
    //                    .build()
    //    );
    //    // @formatter:on
    //}


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/logout").setViewName("logout");
    }
}
