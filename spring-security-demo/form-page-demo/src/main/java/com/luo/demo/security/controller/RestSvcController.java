package com.luo.demo.security.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest Json controller
 *
 * @author luohq
 * @date 2022-02-15
 */
@RestController
@Slf4j
public class RestSvcController {

    @GetMapping("/hello")
    public String hello(Authentication authentication) {
        log.info("cur Authentication: {}", authentication);
        String reply = "Reply Hello, " + authentication.getName() + "!";
        log.info("cur reply: {}", reply);
        return reply;
    }
}
