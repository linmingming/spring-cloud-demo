package com.luo.demo.security.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.endpoint.DefaultRefreshTokenTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2PkceRefreshTokenGrantRequestEntityConverter;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.DefaultOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.web.OAuth2AuthorizedClientRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServletOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.reactive.function.client.WebClient;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * Spring Security配置
 *
 * @author luohq
 * @date 2022-02-18
 */
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http, OidcClientInitiatedLogoutSuccessHandler oidcClientInitiatedLogoutSuccessHandler) throws Exception {
        http

                .headers(headers -> headers
                        .frameOptions(frameOptions -> frameOptions
                                .disable()
                        )
                )
                .authorizeHttpRequests(authorizeRequests -> authorizeRequests
                        .mvcMatchers("/logout", "/front_logout").permitAll()
                        .anyRequest().authenticated()
                )
                .oauth2Login(withDefaults())
                .oauth2Client(withDefaults())
                .logout(logout -> logout
                        .invalidateHttpSession(true)
                        //.logoutSuccessUrl("http://oauth2-server:9000/logout")
                        //.logoutSuccessHandler(new OidcEndSessionLogoutSuccessHandler("http://oauth2-server:9000/logout", "http://oauth2-client1:8081/"))
                        .logoutSuccessHandler(oidcClientInitiatedLogoutSuccessHandler)
                )
                //.addFilterBefore(new FrontChannelLogoutGeneratingFilter("front_logout"), DefaultLoginPageGeneratingFilter.class)
        ;
        return http.build();
    }


    /**
     * 配置WebClient支持使用AuthorizedClient
     *
     * @param authorizedClientManager
     * @return
     */
    @Bean
    WebClient webClient(OAuth2AuthorizedClientManager authorizedClientManager) {
        ServletOAuth2AuthorizedClientExchangeFilterFunction oauth2Client =
                new ServletOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager);
        return WebClient.builder()
                .apply(oauth2Client.oauth2Configuration())
                .build();
    }

    @Bean
    OAuth2AuthorizedClientManager authorizedClientManager(
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2AuthorizedClientRepository authorizedClientRepository) {

        //扩展支持PKCE模式（NONE）下刷新token时添加client_id参数
        DefaultRefreshTokenTokenResponseClient refreshTokenTokenResponseClient = new DefaultRefreshTokenTokenResponseClient();
        //设置自定义参数转换器（解决pkce模式下无client_id参数问题）
        refreshTokenTokenResponseClient.setRequestEntityConverter(new OAuth2PkceRefreshTokenGrantRequestEntityConverter());

        OAuth2AuthorizedClientProvider authorizedClientProvider =
                OAuth2AuthorizedClientProviderBuilder.builder()
                        //client支持code流程
                        .authorizationCode()
                        //client支持refresh_token流程
                        .refreshToken(refreshToken -> refreshToken
                                //扩展支持PKCE模式（NONE）下刷新token时添OAuth2AuthorizationRequestRedirectFilter 加client_id参数
                                .accessTokenResponseClient(refreshTokenTokenResponseClient)
                        )
                        .build();
        DefaultOAuth2AuthorizedClientManager authorizedClientManager = new DefaultOAuth2AuthorizedClientManager(
                clientRegistrationRepository, authorizedClientRepository);
        authorizedClientManager.setAuthorizedClientProvider(authorizedClientProvider);

        return authorizedClientManager;
    }

    /**
     * 单点登录配置
     */
    @Bean
    OidcClientInitiatedLogoutSuccessHandler oidcClientInitiatedLogoutSuccessHandler (ClientRegistrationRepository clientRegistrationRepository) {
        OidcClientInitiatedLogoutSuccessHandler oidcClientInitiatedLogoutSuccessHandler = new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        oidcClientInitiatedLogoutSuccessHandler.setPostLogoutRedirectUri("http://oauth2-client1:8081/");
        return oidcClientInitiatedLogoutSuccessHandler;
    }
}