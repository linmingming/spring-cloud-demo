package com.luo.demo.security.authserver.handler.kapatcha.config;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.impl.NoNoise;
import com.google.code.kaptcha.impl.ShadowGimpy;
import com.google.code.kaptcha.text.impl.DefaultTextCreator;
import com.google.code.kaptcha.util.Config;
import com.luo.demo.security.authserver.handler.kapatcha.KaptchaMathExpTextCreator;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

import static com.google.code.kaptcha.Constants.*;

/**
 * 谷歌验证码配置文件
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-04
 */
@ConfigurationProperties(prefix = KaptchaConfigProps.KAPTCHA_PREFIX)
public class KaptchaConfigProps {

    /**
     * 属性前缀 - 常量定义
     */
    public static final String KAPTCHA_PREFIX = "kaptcha";
    public static final String KAPTCHA_TEXT_PREFIX = "kaptcha.text.";
    public static final String KAPTCHA_MATH_PREFIX = "kaptcha.math.";

    /**
     * 文本验证码配置属性
     */
    private Properties text = new Properties();
    /**
     * 算术表达式验证码配置属性
     */
    private Properties math = new Properties();

    /**
     * 生成并获取文本验证码Kaptcha配置
     *
     * @return Kaptcha配置
     */
    public Config getKaptchaTextConfig() {
        Properties properties = this.convertKaptchaProps(KAPTCHA_TEXT_PREFIX, this.text,
                //设置图片边框
                KAPTCHA_BORDER, "no",
                KAPTCHA_BORDER_COLOR, "220,220,220",
                //设置图片大小
                KAPTCHA_IMAGE_WIDTH, "100",
                KAPTCHA_IMAGE_HEIGHT, "40",
                //字符大小
                KAPTCHA_TEXTPRODUCER_FONT_SIZE, "20",
                //设置字符间距
                KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "8",
                //设置字符颜色
                KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue",
                //验证码字符串长度（即包含多少个字符）
                KAPTCHA_TEXTPRODUCER_CHAR_LENGTH, "4",
                //验证码字符范围（如下为默认值）
                Constants.KAPTCHA_TEXTPRODUCER_CHAR_STRING, "abcde2345678gfynmnpwx",
                //设置字符字体
                KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier",
                //设置对应text生成器
                KAPTCHA_TEXTPRODUCER_IMPL, DefaultTextCreator.class.getName(),
                //设置噪音
                KAPTCHA_NOISE_COLOR, "white",
                KAPTCHA_NOISE_IMPL, NoNoise.class.getName(),
                KAPTCHA_OBSCURIFICATOR_IMPL, ShadowGimpy.class.getName(),
                //session key
                KAPTCHA_SESSION_CONFIG_KEY, KAPTCHA_SESSION_KEY);
        return new Config(properties);
    }

    /**
     * 生成并获取算术表达式验证码Kaptcha配置
     *
     * @return Kaptcha配置
     */
    public Config getKaptchaMathConfig() {
        Properties properties = this.convertKaptchaProps(KAPTCHA_MATH_PREFIX, this.math,
                //设置图片边框
                KAPTCHA_BORDER, "no",
                KAPTCHA_BORDER_COLOR, "220,220,220",
                //设置图片大小
                KAPTCHA_IMAGE_WIDTH, "100",
                KAPTCHA_IMAGE_HEIGHT, "40",
                //字符大小
                KAPTCHA_TEXTPRODUCER_FONT_SIZE, "20",
                //设置字符间距
                KAPTCHA_TEXTPRODUCER_CHAR_SPACE, "5",
                //设置字符颜色
                KAPTCHA_TEXTPRODUCER_FONT_COLOR, "blue",
                //设置字符字体
                KAPTCHA_TEXTPRODUCER_FONT_NAMES, "Arial,Courier",
                //设置对应text生成器
                KAPTCHA_TEXTPRODUCER_IMPL, KaptchaMathExpTextCreator.class.getName(),
                //设置噪音
                KAPTCHA_NOISE_COLOR, "white",
                KAPTCHA_NOISE_IMPL, NoNoise.class.getName(),
                KAPTCHA_OBSCURIFICATOR_IMPL, ShadowGimpy.class.getName(),
                //session key
                KAPTCHA_SESSION_CONFIG_KEY, KAPTCHA_SESSION_KEY);
        return new Config(properties);
    }


    /**
     * 转换Spring属性为Kaptcha Config属性
     *
     * @param springPropsPrefix          spring属性前缀
     * @param springProps                spring属性
     * @param propNameAndDefaultValPairs Kaptcha属性名及默认值列表
     * @return Kaptcha配置属性
     */
    private Properties convertKaptchaProps(String springPropsPrefix, Properties springProps, String... propNameAndDefaultValPairs) {
        Properties kaptchaProps = new Properties();
        for (int i = 0; i < propNameAndDefaultValPairs.length; i += 2) {
            //kaptcha属性名
            String propName = propNameAndDefaultValPairs[i];
            //kaptcha属性默认值
            String propDefaultVal = propNameAndDefaultValPairs[i + 1];
            //对应的spring配置属性名，包含前缀如kaptcha.text. 或 kaptcha.math.
            String springPropName = springPropsPrefix.concat(propName);
            //转换spring属性为kaptcha对应的属性
            kaptchaProps.setProperty(propName, springProps.getProperty(springPropName, propDefaultVal));
        }
        return kaptchaProps;
    }
}
