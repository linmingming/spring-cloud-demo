package com.luo.demo.security.authserver.handler.kapatcha;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Kaptcha 生成结果保存接口
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-14 14:48
 */
public interface KapatchResultSaveService {

    /**
     * 保存图片验证码结果文本（用于后续提交验证）
     *
     * @param resultText 验证码结果文本
     * @param request    请求对象
     * @param response   响应对象
     */
    void saveResult(String resultText, HttpServletRequest request, HttpServletResponse response);


    /**
     * 用于获取之前保存的保存图片验证码结果文本
     *
     * @param request  请求对象
     * @param response 响应对象
     * @return 返回之前缓存的验证码结果文本
     */
    String getResult(HttpServletRequest request, HttpServletResponse response);
}
