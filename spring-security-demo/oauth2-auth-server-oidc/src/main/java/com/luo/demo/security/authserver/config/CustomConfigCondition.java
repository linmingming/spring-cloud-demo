package com.luo.demo.security.authserver.config;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 自定义配置条件
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-14 16:53
 */
public class CustomConfigCondition {

    /**
     * 自定义captcha开启条件（解决使用@ConditionalOnProperty注解无法获取proflie配置文件中的属性问题）
     */
    public static class EnableCaptchaCondition implements Condition {

        @Override
        public boolean matches(final ConditionContext context, final AnnotatedTypeMetadata metadata) {
            final Environment environment = context.getEnvironment();
            String propVal = environment.getProperty(Oauth2ServerProps.PREFIX.concat(".enable-captcha"));
            return Boolean.TRUE.toString().equals(propVal);
        }
    }
}
