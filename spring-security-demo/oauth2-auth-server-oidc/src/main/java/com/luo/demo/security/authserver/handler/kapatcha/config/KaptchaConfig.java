package com.luo.demo.security.authserver.handler.kapatcha.config;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.luo.demo.security.authserver.config.CustomConfigCondition;
import com.luo.demo.security.authserver.handler.kapatcha.DelegatingKapatchaProducer;
import com.luo.demo.security.authserver.handler.kapatcha.KapatchResultSaveService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.lang.Nullable;

/**
 * 谷歌验证码配置文件
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-03-04
 */
//@ConditionalOnProperty(name = "spring.security.oauth2.authserver.enable-captcha")
@Conditional(CustomConfigCondition.EnableCaptchaCondition.class)
@EnableConfigurationProperties(KaptchaConfigProps.class)
public class KaptchaConfig {

    /**
     * Kaptcha配置属性
     */
    private KaptchaConfigProps kaptchaConfigProps;

    public KaptchaConfig(KaptchaConfigProps kaptchaConfigProps) {
        this.kaptchaConfigProps = kaptchaConfigProps;
    }

    /**
     * 字符验证码生成器
     */
    @Bean
    public DefaultKaptcha kaptchaTextProducer() {
        DefaultKaptcha kaptchatextPropcer = new DefaultKaptcha();
        kaptchatextPropcer.setConfig(this.kaptchaConfigProps.getKaptchaTextConfig());
        return kaptchatextPropcer;
    }

    /**
     * 算术表达式验证码生成器
     */
    @Bean
    public DefaultKaptcha kaptchaMathProducer() {
        DefaultKaptcha kaptchaMathProducer = new DefaultKaptcha();
        kaptchaMathProducer.setConfig(this.kaptchaConfigProps.getKaptchaMathConfig());
        return kaptchaMathProducer;
    }

    /**
     * Kaptcha代理对象
     */
    @Bean
    public DelegatingKapatchaProducer delegatingKapatchaProducer(@Nullable KapatchResultSaveService kapatchResultSaveService) {
        return new DelegatingKapatchaProducer(this.kaptchaTextProducer(), this.kaptchaMathProducer(), kapatchResultSaveService);
    }



}
