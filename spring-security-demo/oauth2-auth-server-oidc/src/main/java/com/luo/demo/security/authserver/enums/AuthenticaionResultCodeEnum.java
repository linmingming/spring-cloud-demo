package com.luo.demo.security.authserver.enums;

/**
 * 认证结果响应码枚举
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-21 11:10
 */
public enum AuthenticaionResultCodeEnum {
    SUCCESS(100, "认证成功"),
    FAILURE(101, "认证失败");

    /**
     * 结果码
     */
    private Integer code;
    /**
     * 结果码描述
     */
    private String desc;

    AuthenticaionResultCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
