package com.luo.demo.security.authserver.config;

import com.luo.demo.security.authserver.constant.Oauth2Constants.PROVIDER_SETTINGS;
import com.luo.demo.security.authserver.controller.consent.AuthorizationConsentController;
import com.luo.demo.security.authserver.enums.PasswordEncoderEnum;
import com.luo.demo.security.authserver.handler.oidc.DefaultOidcTokenCustomer;
import com.luo.demo.security.authserver.handler.oidc.DefaultOidcUserInfoMapper;
import com.luo.demo.security.authserver.handler.oidc.OidcCustomProviderConfigurationEndpointFilter;
import com.luo.demo.security.authserver.service.JdbcOidcAuthorizationService;
import com.luo.demo.security.authserver.service.OidcAuthorizationService;
import com.luo.demo.security.authserver.utils.Jwks;
import com.luo.demo.security.authserver.utils.ObjectPostProcessorUtils;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.server.authorization.*;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationCodeAuthenticationProvider;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationProvider;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2CustomAuthorizationCodeAuthenticationProvider;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2CustomClientAuthenticationProvider;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;
import org.springframework.security.oauth2.server.authorization.oidc.web.OidcProviderConfigurationEndpointFilter;
import org.springframework.security.oauth2.server.authorization.web.OAuth2ClientAuthenticationFilter;
import org.springframework.security.oauth2.server.authorization.web.authentication.*;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

/**
 * OAuth2 AuthServer配置
 *
 * @author luo
 * @date 2022-02-17
 */
@Slf4j
@EnableConfigurationProperties(Oauth2ServerProps.class)
@ComponentScan("com.luo.demo.security.authserver.controller.consent")
public class AuthorizationServerConfig {

    private Oauth2ServerProps oauth2ServerProps;

    private DefaultOidcUserInfoMapper.OidcUserInfoMapperExtend oidcUserInfoMapperExtend;
    private DefaultOidcTokenCustomer.AbstractOidcTokenCustomerExtend oidcTokenCustomerExtend;

    public AuthorizationServerConfig(Oauth2ServerProps oauth2ServerProps,
                                     @Nullable DefaultOidcTokenCustomer.AbstractOidcTokenCustomerExtend oidcTokenCustomerExtend,
                                     @Nullable DefaultOidcUserInfoMapper.OidcUserInfoMapperExtend oidcUserInfoMapperExtend) {
        this.oauth2ServerProps = oauth2ServerProps;
        this.oidcTokenCustomerExtend = oidcTokenCustomerExtend;
        this.oidcUserInfoMapperExtend = oidcUserInfoMapperExtend;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http,
                                                                      RegisteredClientRepository registeredClientRepository,
                                                                      OAuth2AuthorizationService authorizationService) throws Exception {
        OAuth2AuthorizationServerConfigurer<HttpSecurity> authorizationServerConfigurer =
                new OAuth2AuthorizationServerConfigurer<>();
        RequestMatcher endpointsMatcher = authorizationServerConfigurer
                .getEndpointsMatcher();

        http
                //仅拦截OAuth2 Authorization Server的相关endpoint
                .requestMatcher(endpointsMatcher)
                //开启请求认证
                .authorizeHttpRequests(authorizeRequests ->
                        authorizeRequests
                                .anyRequest().authenticated()
                )
                //禁用OAuth2 Server相关endpoint的CSRF防御
                .csrf(csrf -> csrf.ignoringRequestMatchers(endpointsMatcher))
                //需开启OAuth2 Resource Server支持OIDC /userinfo 的Bearer accessToken鉴权（否则401）
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                //OAuth2相关设置
                .apply(authorizationServerConfigurer)
                .authorizationEndpoint(authorizationEndpoint -> authorizationEndpoint
                        //设置确认界面uri
                        .consentPage(this.oauth2ServerProps.getConsentPageUrl())
                )
                //OIDC相关设置
                .oidc(oidc -> oidc
                        //UserInfo endpoint相关设置
                        .userInfoEndpoint(userInfo -> userInfo
                                //设置默认用户信息转换器
                                .userInfoMapper(new DefaultOidcUserInfoMapper(this.oidcUserInfoMapperExtend)))
                )
                //替换OidcProviderConfigurationEndpointFilter默认实现为OidcOpConfigurationEndpointFilter
                .withObjectPostProcessor(ObjectPostProcessorUtils.objectPostReturnNewObj(
                        OncePerRequestFilter.class,
                        OidcProviderConfigurationEndpointFilter.class,
                        new OidcCustomProviderConfigurationEndpointFilter(this.providerSettings())))
                //扩展Oauth2 client认证 参数转换器 - 支持RefreshToken无需client_secret认证
                .withObjectPostProcessor(ObjectPostProcessorUtils.objectPostAppendHandle(
                        OncePerRequestFilter.class,
                        OAuth2ClientAuthenticationFilter.class,
                        oAuth2ClientAuthenticationFilter -> {
                            log.debug("expand {} with append new converter: {}", oAuth2ClientAuthenticationFilter.getClass().getSimpleName(), PublicClientRefreshTokenAuthenticationConverter.class.getSimpleName());
                            //扩展Oauth2 client认证 参数转换器request -> OAuth2ClientAuthenticationToken
                            oAuth2ClientAuthenticationFilter.setAuthenticationConverter(new DelegatingAuthenticationConverter(
                                    Arrays.asList(
                                            new JwtClientAssertionAuthenticationConverter(),
                                            new ClientSecretBasicAuthenticationConverter(),
                                            new ClientSecretPostAuthenticationConverter(),
                                            /** 添加自定义RefreshToken请求解析器（支持不提供client_secret刷新token） */
                                            new PublicClientRefreshTokenAuthenticationConverter(),
                                            new PublicClientAuthenticationConverter())));
                        }))
                //扩展OAuth2 Token端点 客户端认证逻辑 - 支持refresh_token不提供client_secret认证（支持PKCE code模式）
                .withObjectPostProcessor(ObjectPostProcessorUtils.objectPostReturnNewObj(
                        AuthenticationProvider.class,
                        OAuth2ClientAuthenticationProvider.class,
                        new OAuth2CustomClientAuthenticationProvider(registeredClientRepository, authorizationService)))
                //扩展OAuth2 Token端点 - 支持在PKCE模式下也生成refresh_token
                .withObjectPostProcessor(ObjectPostProcessorUtils.objectPostConvertObj(
                        AuthenticationProvider.class,
                        OAuth2AuthorizationCodeAuthenticationProvider.class,
                        oAuth2AuthorizationCodeAuthenticationProvider -> new OAuth2CustomAuthorizationCodeAuthenticationProvider(authorizationService, http.getSharedObject(JwtEncoder.class))));
        //return http.formLogin(Customizer.withDefaults()).build();
        return http.formLogin(form -> form
                //替换默认配置（兼容自定义loginPageUrl时的oauth2登录跳转）
                .loginPage(this.oauth2ServerProps.getLoginPageUrl())
        ).build();
    }



    ////@ConditionalOnProperty(name = "spring.security.oauth2.authserver.auto-config-consent-page", matchIfMissing = true)
    //@ComponentScan(basePackageClasses = AuthorizationConsentController.class)
    //@Configuration
    //public class ConsentPageConfig {
    //
    //}

    /**
     * 自定义JWT编码上下文<br/>
     * 填充jwt.claims.sid为当前OP sessionId
     */
    @Bean
    public OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomer() {
        return new DefaultOidcTokenCustomer(this.oidcTokenCustomerExtend);
    }

    @Bean
    @ConditionalOnMissingBean
    public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate) {
        JdbcRegisteredClientRepository registeredClientRepository = new JdbcRegisteredClientRepository(jdbcTemplate);
        //测试时可开启，以便生成ClientRegistration
        //this.saveClientRegistration(registeredClientRepository);
        return registeredClientRepository;
    }




    @Bean
    @ConditionalOnMissingBean
    public OidcAuthorizationService authorizationService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
        return new JdbcOidcAuthorizationService(jdbcTemplate, registeredClientRepository);
    }

    @Bean
    @ConditionalOnMissingBean
    public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
        return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
    }

    @Bean
    public JWKSource<SecurityContext> jwkSource() throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        RSAKey rsaKey = Jwks.convertRsaKey(this.oauth2ServerProps);
        JWKSet jwkSet = new JWKSet(rsaKey);
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

    /**
     * 配置 Provider元数据
     */
    @Bean
    public ProviderSettings providerSettings() {
        return ProviderSettings.builder()
                .issuer(this.oauth2ServerProps.getIssuer())
                .setting(PROVIDER_SETTINGS.END_SESSION_ENDPOINT, this.oauth2ServerProps.getEndSessionEndpoint())
                .authorizationEndpoint(this.oauth2ServerProps.getAuthorizationEndpoint())
                .tokenEndpoint(this.oauth2ServerProps.getTokenEndpoint())
                .jwkSetEndpoint(this.oauth2ServerProps.getJwkSetEndpoint())
                .oidcUserInfoEndpoint(this.oauth2ServerProps.getOidcUserInfoEndpoint())
                .tokenIntrospectionEndpoint(this.oauth2ServerProps.getTokenIntrospectionEndpoint())
                .tokenRevocationEndpoint(this.oauth2ServerProps.getTokenRevocationEndpoint())
                .build();
    }


    /**
     * 设置密码解析器
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        String idForEncoder = PasswordEncoderEnum.getDefaultPasswordEncoderEnum(this.oauth2ServerProps.getPasswordEncoder()).getId();
        log.debug("default password encoder: {}", idForEncoder);
        return new DelegatingPasswordEncoder(idForEncoder, PasswordEncoderEnum.toIdToPasswordEncoderMap());
    }

}
