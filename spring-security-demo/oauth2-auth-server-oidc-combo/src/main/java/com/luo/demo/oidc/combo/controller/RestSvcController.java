package com.luo.demo.oidc.combo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class RestSvcController {

    private List<String> articles = Arrays.asList("Article 1", "Article 2", "Article 3");

    @GetMapping("/articles")
    public List<String> getArticles() {
        log.info("[GET]get articles, result: {}", articles);
        return this.articles;
    }

    @PostMapping("/articles")
    public List<String> getArticlesPost() {
        log.info("[POST]get articles, result: {}", articles);
        return this.articles;
    }
}