package com.luo.demo.oidc.resource.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
@RequestMapping("/api/articles")
public class RestSvcController {

    @GetMapping
    public String[] getArticles() {
        return new String[]{"Article 1", "Article 2", "Article 3"};
    }

    @PostMapping("/post")
    public String[] getArticlesPost() {
        return new String[]{"Article 1", "Article 2", "Article 3"};
    }
}
