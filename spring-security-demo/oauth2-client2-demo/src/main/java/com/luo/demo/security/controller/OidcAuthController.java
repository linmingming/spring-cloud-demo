package com.luo.demo.security.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 页面controller
 *
 * @author luohq
 * @version 1.0.0
 * @date 2022-02-14 14:52
 */
@RestController
@Slf4j
public class OidcAuthController {

    //TODO 自定义sessionRegistry实现
    @Resource
    private SessionRegistry sessionRegistry;

    @GetMapping("/front_logout")
    public void frontLogout(@RequestParam String sid, @RequestParam String iss) throws IOException {
        //TODO 根据iss和sid查找指定的http  session，然后注销
        log.info("front logout, param: sid={}, iss={}", sid, iss);
        //sessionRegistry.getAllPrincipals()
    }

    @PostMapping("/back_logout")
    public void backLogout(@RequestParam(name = "logout_token") String logoutToken) {
        log.info("back logout, param: logout_token={}", logoutToken);
    }
}
