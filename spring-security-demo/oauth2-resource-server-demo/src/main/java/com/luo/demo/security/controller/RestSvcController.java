package com.luo.demo.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST服务 - Controller
 *
 * @author luohq
 * @date 2022-02-18
 */
@RestController
public class RestSvcController {

    private String[] articles = {"Article 1", "Article 2", "Article 3"};

    @GetMapping("/articles")
    public String[] getArticles() {
        return this.articles;
    }

    @PostMapping("/articles")
    public String[] getArticlesPost() {
        return this.articles;
    }
}
