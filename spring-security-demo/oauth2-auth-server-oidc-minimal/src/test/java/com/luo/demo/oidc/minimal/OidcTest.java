package com.luo.demo.oidc.minimal;

import com.luo.demo.security.authserver.constant.Oauth2Constants;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;
import org.springframework.security.oauth2.server.authorization.config.TokenSettings;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.UUID;

/**
 * OIDC 相关测试
 *
 * @author luohq
 * @date 2022-03-05 11:22
 */
@Slf4j
@SpringBootTest
@ActiveProfiles("oidc")
public class OidcTest {

    @Resource
    private JdbcRegisteredClientRepository registeredClientRepository;

    @Resource
    private PasswordEncoder passwordEncoder;

    @ParameterizedTest
    @ValueSource(strings = {"123456", "luo-oauth2-client1-secret", "luo-oauth2-client2-secret"})
    void testPasswordEncoder(String rawPassword) {
        log.info("rawPassword: {}, encodedPassword: {}", rawPassword, this.passwordEncoder.encode(rawPassword));
    }

    @Test
    //有条件测试，避免直接执行测试用例而导致无效插入
    @EnabledIfSystemProperty(named = "testRegOidcClient", matches = "true")
    void regOidcClient() {
        //RegisteredClient registeredClient_client1 = RegisteredClient.withId(UUID.randomUUID().toString())
        //        .clientName("luo-oauth2-client1 - 客户端")
        //        .clientId("luo-oauth2-client1")
        //        .clientSecret("{bcrypt}$2a$10$LgGXHSU2Fh/dCLIwrOetiOnCK3Zypeo588EpAOQeJAnT0kdiia6em")
        //        .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
        //        .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
        //        .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
        //        .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
        //        .redirectUri("http://oauth2-client1:8081/login/oauth2/code/luo-oauth2-client1")
        //        .scope(OidcScopes.OPENID)
        //        .scope(OidcScopes.PHONE)
        //        .scope(OidcScopes.EMAIL)
        //        .scope(OidcScopes.PROFILE)
        //        .scope("articles.read")
        //        .scope("roles")
        //        .clientSettings(ClientSettings.builder()
        //                .requireAuthorizationConsent(true)
        //                //.requireProofKey(true)
        //                .setting(Oauth2Constants.FRONTCHANNEL_LOGOUT_URI, "http://oauth2-client1:8081/front_logout")
        //                .build())
        //        .build();


        RegisteredClient registeredClient_client1 = RegisteredClient.withId(UUID.randomUUID().toString())
                //客户端名称
                .clientName("luo-oauth2-client1 - 客户端")
                //客户端ID和Secret（需在Client端进行配置），且Secret需使用对应的PasswordEncoder进行编码
                .clientId("luo-oauth2-client1")
                .clientSecret("{bcrypt}$2a$10$LgGXHSU2Fh/dCLIwrOetiOnCK3Zypeo588EpAOQeJAnT0kdiia6em")
                //客户端认证方法
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                //客户端认证方none - 若开启PKCE认证，则需添加none认证方法，否则
                .clientAuthenticationMethod(ClientAuthenticationMethod.NONE)
                //支持的OAuth2授权模式（authorization_code和refresh_token）
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                //在OP端认证成功后的回调（重定向回RP端）URI
                .redirectUri("http://oauth2-client1:8081/login/oauth2/code/luo-oauth2-client1")
                //添加Client端对应的权限scope
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PHONE)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PROFILE)
                //支持PKCE模式下（无client_secret）获取refresh_token
                .scope(OIDCScopeValue.OFFLINE_ACCESS.getValue())
                .scope("articles.read")
                .scope("roles")
                //Client相关设置
                .clientSettings(ClientSettings.builder()
                        //是否需要展示权限确认页面
                        .requireAuthorizationConsent(true)
                        //是否需要开启PKCE模式（SPA建议开启）
                        .requireProofKey(true)

                        //如下两项jwtSetUrl、tokenEndpointAuthenticationSigningAlgorithm适用于认证方法private_key_jwt和client_secret_jwt，
                        //以上使用的client_secret_post和client_secret_basic可不用配置
                        //覆盖Client端的jwksSetUrl
                        //.jwkSetUrl(null)
                        //设置client认证（private_key_jwt和client_secret_jwt）JWT签名算法
                        //.tokenEndpointAuthenticationSigningAlgorithm(SignatureAlgorithm.RS256)

                        //注：idToken、accessToke均使用RS256签名算法，目前不可配置，参见：JwtUtils.headers方法
                        //自定义配置（如自定义OIDC协议中RP的前端、后端登出URI）
                        .setting(Oauth2Constants.CLIENT_SETTINGS.FRONTCHANNEL_LOGOUT_URI, "http://oauth2-client1:8081/front_logout")
                        .setting(Oauth2Constants.CLIENT_SETTINGS.BACKCHANNEL_LOGOUT_URI, "http://oauth2-client1:8081/back_logout")
                        .build())
                //Token相关设置
                .tokenSettings(TokenSettings.builder()
                        //accessToken生存时长（即超过多久失效，默认5分钟）
                        .accessTokenTimeToLive(Duration.ofMinutes(5))
                        //refreshToken生存时长（即超过多久失效，默认60分钟）
                        .refreshTokenTimeToLive(Duration.ofMinutes(60))
                        //执行刷新token流程时，是否返回新的refreshToken（默认true即重用refreshToken），
                        //true则重用之前的refreshToken，false则生成新的refreshToken及生存时长
                        .reuseRefreshTokens(false)
                        //设置idToken签名算法 TODO 参见 OidcClientRegistrationEndpointFilter 逻辑
                        .idTokenSignatureAlgorithm(SignatureAlgorithm.RS256)
                        //支持PKCE模式下（无client_secret）执行refresh_token流程
                        .setting(Oauth2Constants.TOKEN_SETTINGS.ALLOW_PUBLIC_CLIENT_REFRESH_TOKEN, true)
                        //注：idToken默认生存时长30分钟，目前不可配置，参见：JwtUtils.idTokenClaims方法
                        //注：code有效时长5分钟，目前不可配置，参见：OAuth2AuthorizationCodeRequestAuthenticationProvider.generateAuthorizationCode
                        //注：session时长 > refreshToken刷新时长
                        //TODO 注：session时长  remember-me时长
                        .build())
                .build();


        RegisteredClient registeredClient_client2 = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientName("luo-oauth2-client2 - 客户端")
                .clientId("luo-oauth2-client2")
                .clientSecret("{bcrypt}$2a$10$xw.pBLQitbNdAAyBhuBAo.IIP8dbDHcXF5c1YNIDqhhcC18cmNGo2")
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_POST)
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .redirectUri("http://oauth2-client2:8082/login/oauth2/code/luo-oauth2-client2")
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PHONE)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PROFILE)
                .scope("articles.read")
                .scope("roles")
                .clientSettings(ClientSettings.builder()
                        .requireAuthorizationConsent(true)
                        //.requireProofKey(true)
                        .setting(Oauth2Constants.CLIENT_SETTINGS.FRONTCHANNEL_LOGOUT_URI, "http://oauth2-client2:8082/front_logout")
                        .build())
                .build();


        registeredClientRepository.save(registeredClient_client1);
        registeredClientRepository.save(registeredClient_client2);

    }

}

