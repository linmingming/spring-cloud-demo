package com.luo.demo.oidc.third;

import com.luo.demo.security.authserver.anno.EnableOidcAuthorizationServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * OIDC Server启动类
 *
 * @author luo
 * @date 2022-02-17
 */
@EnableOidcAuthorizationServer
@SpringBootApplication
public class OidcAuthServerLoginThirdApplication {

	public static void main(String[] args) {
		SpringApplication.run(OidcAuthServerLoginThirdApplication.class, args);
	}

}