package com.luo.demo.api.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 新增设备
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
@Data
@Builder
@Schema(description = "新增设备参数")
public class DeviceAddDto {
    /**
     * 设备名称
     */
    @Schema(description = "设备名称")
    @NotBlank
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    @Schema(description = "设备类型（1:电脑, 2:手机, 3:平板）", example = "1")
    @NotNull
    @Range(min = 1, max = 3)
    private Integer type;
}
