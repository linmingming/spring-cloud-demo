package com.luo.demo.api.model.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 设备信息
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
@Data
@Builder
@ApiModel("设备信息")
public class DeviceInfo {
    /**
     * 设备ID
     */
    @ApiModelProperty(value = "设备ID", required = true)
    private Long id;
    /**
     * 设备名称
     */
    @ApiModelProperty(value = "设备名称", required = true)
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    @ApiModelProperty(value = "设备类型（1:电脑, 2:手机, 3:平板）", required = true)
    private Integer type;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;
}
