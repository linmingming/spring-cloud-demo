package com.luo.demo.api.model.dto;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * 新增用户参数
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
public class UserAddDto {
    /**
     * 用户名称
     */
    @NotBlank
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @NotNull
    @Range(min = 1, max = 2)
    private Integer sex;

    /**
     * 设备列表
     */
    @NotEmpty
    private List<DeviceAddDto> deviceInfoList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public List<DeviceAddDto> getDeviceInfoList() {
        return deviceInfoList;
    }

    public void setDeviceInfoList(List<DeviceAddDto> deviceInfoList) {
        this.deviceInfoList = deviceInfoList;
    }

    @Override
    public String toString() {
        return "UserAddDto{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                ", deviceInfoList=" + deviceInfoList +
                '}';
    }
}
