package com.luo.demo.api.model.dto;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 新增设备参数
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
public class DeviceAddDto {
    /**
     * 设备名称
     */
    @NotBlank
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    @NotNull
    @Range(min = 1, max = 3)
    private Integer type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "DeviceAddDto{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
