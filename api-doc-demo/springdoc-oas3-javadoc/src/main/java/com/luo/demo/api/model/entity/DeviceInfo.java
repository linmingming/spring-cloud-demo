package com.luo.demo.api.model.entity;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * 设备信息
 *
 * @author luohq
 * @date 2022-01-15 12:23
 */
public class DeviceInfo {
    /**
     * 设备ID
     */
    private Long id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备类型（1:电脑, 2:手机, 3:平板）
     */
    private Integer type;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

    private DeviceInfo(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setType(builder.type);
        setCreateTime(builder.createTime);
        setUpdateTime(builder.updateTime);
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }


    public static final class Builder {
        private Long id;
        private String name;
        private Integer type;
        private LocalDateTime createTime;
        private LocalDateTime updateTime;

        private Builder() {
        }

        public Builder id(Long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder type(Integer val) {
            type = val;
            return this;
        }

        public Builder createTime(LocalDateTime val) {
            createTime = val;
            return this;
        }

        public Builder updateTime(LocalDateTime val) {
            updateTime = val;
            return this;
        }

        public DeviceInfo build() {
            return new DeviceInfo(this);
        }
    }
}
