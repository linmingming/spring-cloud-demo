package com.luo.demo.api.controller.user;

import com.luo.demo.api.model.dto.UserAddDto;
import com.luo.demo.api.model.dto.UserEditDto;
import com.luo.demo.api.model.dto.UserQueryDto;
import com.luo.demo.api.model.entity.DeviceInfo;
import com.luo.demo.api.model.entity.UserInfo;
import com.luo.demo.sc.base.model.result.RespResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 用户信息管理
 *
 * @apiNote 用户信息管理
 * @author luohq
 * @date 2022-01-15 12:29
 */
@RestController
@RequestMapping("/users")
@Validated
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final Long TOTAL_DEFAULT = 3L;
    private final Long ID_DEFAULT = 1L;

    /**
     * 查询用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     * @apiNote 根据用户ID查询用户信息
     */
    @GetMapping("/{id}")
    public RespResult<UserInfo> getUser(@NotNull @PathVariable Long id) {
        log.info("get user, param: id={}", id);
        return RespResult.successData(this.buildUser(id));
    }

    /**
     * 查询用户信息列表
     *
     * @param userQueryDto 查询参数
     * @return 用户列表
     */
    @GetMapping
    public RespResult<UserInfo> getUsers(@Validated UserQueryDto userQueryDto) {
        log.info("get users, param: {}", userQueryDto);
        return RespResult.successRows(TOTAL_DEFAULT, this.buildUsers(Optional.ofNullable(userQueryDto.getId()).orElse(ID_DEFAULT), TOTAL_DEFAULT.intValue()));
    }

    /**
     * 新增用户及设备绑定信息
     *
     * @param userAddDto 新增参数
     * @return 响应结果
     */
    @PostMapping
    public RespResult<Integer> addUser(@Validated @RequestBody UserAddDto userAddDto) {
        log.info("add user, param: {}", userAddDto);
        return RespResult.successData(1);
    }

    /**
     * 修改用户及设备绑定信息
     *
     * @param userEditDto 修改参数
     * @return 响应结果
     */
    @PutMapping
    public RespResult<Integer> updateUser(@Validated @RequestBody UserEditDto userEditDto) {
        log.info("update user, param: {}", userEditDto);
        return RespResult.successData(1);
    }

    /**
     * 删除用户信息
     *
     * @param ids 用户ID列表
     * @return 响应结果
     * @apiNote 根据用户ID列表删除用户信息
     */
    @DeleteMapping("/{ids}")
    public RespResult<Integer> deleteUsers(@NotEmpty @PathVariable List<Long> ids) {
        log.info("delete users, param: ids={}", ids);
        return RespResult.successData(ids.size());
    }


    private UserInfo buildUser(Long id) {
        return this.buildUsers(id, 1).get(0);
    }

    private List<UserInfo> buildUsers(Long id, Integer count) {
        LocalDateTime now = LocalDateTime.now();
        return IntStream.range(0, count).mapToObj(index -> {
            Long curId = id + index;
            return UserInfo.builder()
                    .id(curId)
                    .name("用户-" + curId)
                    .sex(curId.intValue() % 2)
                    .createTime(now)
                    .updateTime(now)
                    .deviceInfoList(this.buildDevices(id, new Random().nextInt(10)))
                    .build();
        }).collect(Collectors.toList());

    }

    private List<DeviceInfo> buildDevices(Long id, Integer count) {
        LocalDateTime now = LocalDateTime.now();
        return IntStream.range(0, count).mapToObj(index -> {
            Long curId = id + index;
            return DeviceInfo.builder()
                    .id(curId)
                    .name("设备-" + curId)
                    .type(curId.intValue() % 3)
                    .createTime(now)
                    .updateTime(now)
                    .build();
        }).collect(Collectors.toList());
    }
}
