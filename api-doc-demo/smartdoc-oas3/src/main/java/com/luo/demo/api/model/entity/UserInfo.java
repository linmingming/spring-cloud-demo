package com.luo.demo.api.model.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户信息
 *
 * @author luohq
 * @date 2022-01-15 12:21
 */
@Data
@Builder
public class UserInfo {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名称
     */
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    private Integer sex;
    /**
     * 设备列表
     */
    private List<DeviceInfo> deviceInfoList;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;

}
