package com.luo.demo.api.model.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 用户查询参数
 *
 * @author luohq
 * @date 2022-01-15 13:07
 */
@Data
public class UserQueryDto {
    /**
     * 用户ID
     */
    private Long id;
    /**
     * 用户名称
     */
    @Size(min = 1, max = 30)
    private String name;
    /**
     * 用户性别（1:男，2:女）
     */
    @Range(min = 1, max = 2)
    private Integer sex;
    /**
     * 起始创建日期
     */
    private LocalDateTime createTimeStart;
    /**
     * 结束创建日期
     */
    private LocalDateTime createTimeEnd;


}
