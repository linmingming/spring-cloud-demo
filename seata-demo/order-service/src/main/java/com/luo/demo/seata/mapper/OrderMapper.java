package com.luo.demo.seata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.luo.demo.seata.model.entity.Order;

/**
 * <p>
 * 订单信息 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2021-11-02
 */
public interface OrderMapper extends BaseMapper<Order> {

}
