package com.luo.demo.seata.junit5;

import com.luo.demo.seata.base.BaseTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;

/**
 * Junit5 方法顺序测试
 *
 * @author luohq
 * @date 2021-12-18 10:48
 */
@Order(2)
//@TestClassOrder(ClassOrderer.OrderAnnotation.class)
//@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
class Junit5Test2 extends BaseTest {

    @Order(1)
    @Test
    void test1() {
        log.info("TEST_2_1");
    }

    @Order(2)
    @Test
    void test2() {
        log.info("TEST_2_2");
    }
}
