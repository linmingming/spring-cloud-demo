package com.luo.demo.seata.mapper;

import com.luo.demo.seata.model.entity.Storage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 库存信息 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2021-11-02
 */
public interface StorageMapper extends BaseMapper<Storage> {

}
