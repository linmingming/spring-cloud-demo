package com.luo.demo.sc.scs.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.Message;

import java.util.function.Consumer;

/**
 * biz1 Message消费者
 *
 * @author luohq
 * @date 2021-12-23 09:33
 */
@Slf4j
public class Biz1MessageConsumer implements Consumer<Message<String>> {

    @Override
    public void accept(Message<String> message) {
        log.info("[biz1Consumer] RECV MSG: {}, FROM PARTITION: {}, ", message.getPayload(), message.getHeaders().get(AmqpHeaders.CONSUMER_QUEUE));
    }

}
