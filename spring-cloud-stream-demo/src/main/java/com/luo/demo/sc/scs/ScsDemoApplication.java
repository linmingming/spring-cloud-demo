package com.luo.demo.sc.scs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Cloud Stream 示例启动类
 *
 * @author luohq
 * @date 2021-12-21
 */
@SpringBootApplication
public class ScsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScsDemoApplication.class, args);
    }

}
