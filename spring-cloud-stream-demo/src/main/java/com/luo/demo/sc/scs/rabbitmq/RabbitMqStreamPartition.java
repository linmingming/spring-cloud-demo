package com.luo.demo.sc.scs.rabbitmq;

import com.luo.demo.sc.base.model.result.RespResult;
import com.luo.demo.sc.scs.common.Biz1Consumer;
import com.luo.demo.sc.scs.common.Biz1MessageConsumer;
import com.luo.demo.sc.scs.common.Biz2Consumer;
import com.luo.demo.sc.scs.common.BizTransFunc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * RabbitMq SCS - demo
 *
 * @author luohq
 * @date 2021-12-21 15:46
 */
@Profile("rabbitmq-scs-partition")
@Configuration
@Slf4j
@Controller
public class RabbitMqStreamPartition {

    /**
     * SCS桥接器（发送消息）
     */
    @Resource
    private StreamBridge streamBridge;


    /**
     * @return 定义biz1消息接受者
     */
    @Bean
    public Consumer<Message<String>> biz1Consumer() {
        return new Biz1MessageConsumer();
    }

    /**
     * 向绑定名称发送消息
     *
     * @param bizName 绑定名称（对应**-out-0形式）
     * @param msg 消息分区键值
     * @param msg 消息内容
     * @return 发送结果
     */
    @PostMapping("/rabbit/send/{bizName}/{partitionKey}/{msg}")
    @ResponseBody
    public RespResult sendMsg(@PathVariable String bizName, @PathVariable String partitionKey, @PathVariable String msg) {
        try {
            log.info("发送rabbitMq分区消息：bizName={}, partitionKey={}, msg={}", bizName, partitionKey, msg);
            Boolean sendResult = this.streamBridge.send(
                    bizName,
                    MessageBuilder.withPayload(msg).setHeader("partitionKey", partitionKey).build()
            );
            log.info("发送rabbitMq消息，结果：{}", sendResult);
            return RespResult.success();
        } catch (Throwable ex) {
            log.error("发送rabbitMq消息 - 异常！", ex);
            return RespResult.failed();
        }
    }

}
