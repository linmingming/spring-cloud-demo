package com.luo.demo.sc.scs.common;

import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Supplier;

/**
 * biz2对应的消息生产者
 *
 * @author luohq
 * @date 2021-12-22 10:03
 */
@Slf4j
public class Biz2Producer implements Supplier<String> {

    @Override
    public String get() {
        String message = "hello biz2 - ".concat(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        log.info("[biz2Producer] SEND MSG: {}", message);
        return message;
    }
}
