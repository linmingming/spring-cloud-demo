package com.luo.demo.sc.scs.rabbitmq;

import com.luo.demo.sc.base.model.result.RespResult;
import com.luo.demo.sc.scs.common.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * RabbitMq SCS - demo
 *
 * @author luohq
 * @date 2021-12-21 15:46
 */
@Profile("rabbitmq-scs")
@Configuration
@Slf4j
@Controller
public class RabbitMqStream {

    /**
     * SCS桥接器（发送消息）
     */
    @Resource
    private StreamBridge streamBridge;


    /**
     * @return 定义biz1消息接受者
     */
    @Bean
    public Consumer<String> biz1Consumer() {
        return new Biz1Consumer();
    }

    /**
     * @return 定义biz2消息接受者
     */
    @Bean
    public Consumer<String> biz2Consumer() {
        return new Biz2Consumer();
//        return message -> {
//            log.info("[biz2Consumer] RECV MSG: {}", message);
//        };
    }

//    /**
//     * @return 定义biz1消息发送者
//     */
//    @Bean
//    public Supplier<String> biz1Producer() {
//        return new Biz1Producer();
////        return () -> {
////            String message = "hello biz1 - ".concat(LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
////            log.info("[biz1Producer] SEND MSG: {}", message);
////            return message;
////        };
//    }
//
//    /**
//     * @return 定义biz2消息发送者
//     */
//    @Bean
//    public Supplier<String> biz2Producer() {
//        return new Biz2Producer();
//    }


    /**
     * @return 定义bizTrans消息转发
     */
    @Bean
    public Function<String, String> bizTrans() {
        return new BizTransFunc();
//        return (message) -> {
//            log.info("[bizTrans] RECV MSG: {}", message);
//            String transMessage = String.format("from biz1 to biz2: %s", message);
//            log.info("[bizTrans] SEND MSG: {}", transMessage);
//            return transMessage;
//        };
    }

    /**
     * 向绑定名称发送消息
     *
     * @param bizName 绑定名称（对应**-out-0形式）
     * @param msg 消息内容
     * @return 发送结果
     */
    @PostMapping("/rabbit/send/{bizName}/{msg}")
    @ResponseBody
    public RespResult sendMsg(@PathVariable String bizName, @PathVariable String msg) {
        try {
            log.info("发送rabbitMq消息：bizName={}, msg={}", bizName, msg);
            Boolean sendResult = this.streamBridge.send(bizName, msg);
            log.info("发送rabbitMq消息，结果：{}", sendResult);
            return RespResult.success();
        } catch (Throwable ex) {
            log.error("发送rabbitMq消息 - 异常！", ex);
            return RespResult.failed();
        }
    }

}
