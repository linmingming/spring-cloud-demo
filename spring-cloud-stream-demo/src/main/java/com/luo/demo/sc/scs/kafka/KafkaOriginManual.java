package com.luo.demo.sc.scs.kafka;

import com.luo.demo.sc.base.model.result.RespResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * kafka - 原生示例（手动提交已消费消息）
 *
 * @author luohq
 * @date 2022-01-02 17:24
 */
@Profile("kafka-origin-manual")
@Configuration
@Slf4j
@Controller
public class KafkaOriginManual {

    /**
     * Kafka模板（发送消息）
     */
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 定义biz1消息接收者
     *
     * @param message
     * @kafka.topic topic1
     * @kafka.group group1
     */
    @KafkaListener(
            id = "biz1-${spring.kafka.biz1.consumer.group}",
            groupId = "${spring.kafka.biz1.consumer.group}",
            //仅在多partition单个消费者时，用于多线程消费消息（concurrency <= partition数量）
            //当存在多个消费者时，即便设置concurrency > 1也仅有唯一消费线程生效
            concurrency = "${spring.kafka.biz1.consumer.concurrency}",
            topics = "${spring.kafka.biz1.topic}")
    public void biz1Consumer(String message, Acknowledgment ack) {
        log.info("[biz1Consumer] RECV MSG: {}", message);
        //确认单当前消息（及之前的消息）offset均已被消费完成
        ack.acknowledge();
        //拒绝当前消息（此方法仅适用于listener.type=record）
        //当前poll查询出的剩余消息记录均被抛弃，
        //且当前消费线程在阻塞指定sleep（如下3000毫秒）后重新调用poll获取待消费消息（包括之前poll被抛弃的消息）
        //ack.nack(3000)
    }


    /**
     * 定义biz2消息接收者
     *
     * @param message
     * @kafka.topic topic2
     * @kafka.group group2
     */
    @KafkaListener(
            id = "biz2-${spring.kafka.biz2.consumer.group}",
            groupId = "${spring.kafka.biz2.consumer.group}",
            //消费指定分区
            topicPartitions = {
                    @TopicPartition(topic = "${spring.kafka.biz2.topic}", partitions = "${spring.kafka.biz2.consumer.partitions}")
            })
    public void biz2Consumer(String message, Acknowledgment ack) {
        log.info("[biz2Consumer] RECV MSG: {}", message);
        //确认单当前消息（及之前的消息）offset均已被消费完成
        ack.acknowledge();
    }




    /**
     * 发送消息
     *
     * @param topic kafka主题
     * @param msg   消息内容
     * @return 发送结果
     */
    @PostMapping("/kafka/send/{topic}/{msg}/{partition}")
    @ResponseBody
    public RespResult sendMsg(@PathVariable String topic, @PathVariable String msg, @PathVariable Integer partition) {
        try {
            log.info("发送kafka消息：topic={}, msg={}", topic, msg);
            this.kafkaTemplate.send(topic, msg)
                    .addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
                        @Override
                        public void onSuccess(SendResult<String, String> result) {
                            log.info("发送kafka消息成功 - topic={}, partition={}, msg={}",
                                    result.getProducerRecord().topic(),
                                    result.getProducerRecord().partition(),
                                    result.getProducerRecord().value());
                        }

                        @Override
                        public void onFailure(Throwable ex) {
                            log.error("发送kafka消息失败!", ex);
                        }

                    });
            return RespResult.success();
        } catch (Throwable ex) {
            log.error("发送rabbitMq消息 - 异常！", ex);
            return RespResult.failed();
        }
    }


}
