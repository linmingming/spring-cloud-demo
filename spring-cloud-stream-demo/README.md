

# RabbitMq测试
## RabbitMq普通destination + group
## RabbitMq分区
### 消费者1 - 启动方式
```based
java -jar spring-cloud-stream-demo-1.0.0-SNAPSHOT.jar --spring.profiles.active=rabbitmq-scs-partition --server.port=8081 --spring.cloud.stream.bindings.biz1Consumer-in-0.consumer.instanceIndex=0
```
### 消费者2 - 启动方式
```based
java -jar spring-cloud-stream-demo-1.0.0-SNAPSHOT.jar --spring.profiles.active=rabbitmq-scs-partition --server.port=8082 --spring.cloud.stream.bindings.biz1Consumer-in-0.consumer.instanceIndex=1
```
### 消费者3 - 启动方式
```based
java -jar spring-cloud-stream-demo-1.0.0-SNAPSHOT.jar --spring.profiles.active=rabbitmq-scs-partition --server.port=8083 --spring.cloud.stream.bindings.biz1Consumer-in-0.consumer.instanceIndex=2
```
